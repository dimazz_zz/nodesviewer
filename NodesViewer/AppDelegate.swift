//
//  AppDelegate.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 24.04.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DatabaseManager.configureMigration()
        DatabaseManager.shared.setupDefaultDatabase()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
        return true
    }
}
