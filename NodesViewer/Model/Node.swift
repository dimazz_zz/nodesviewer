//
//  Node.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 24.04.2021.
//

import Foundation
import RealmSwift

class Node: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var parent: Node?
    @objc dynamic private(set) var deleted: Bool = false
    var children = List<Node>()

    override init() {
        super.init()
    }
    
    required init(id: String, name: String, children: [Node] = [], parent: Node? = nil, deleted: Bool = false) {
        self.id = id
        self.name = name
        self.children.append(objectsIn: children)
        self.parent = parent
        super.init()
        self.children.forEach {$0.parent = self}
        if deleted {
            delete()
        }
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        return id == (object as? Node)?.id ?? ""
    }
    
    // MARK: - Public
    func clone() -> Node {
        let copyNode = Node(id: id, name: name, parent: parent, deleted: deleted)
        for child in children {
            copyNode.appendChild(child.clone())
        }
        return copyNode
    }
    
    func update(withNode node: Node) {
        name = node.name
        if node.deleted {
            delete()
        }
    }

    var depth: Int {
        var depth = 0
        if let parent = parent {
            depth = 1;
            depth += parent.depth;
        }
        return depth;
    }
    
    func delete() {
        deleted = true
        children.forEach {$0.delete()}
    }

    func appendChild(_ node: Node) {
        node.parent = self
        children.append(node)
    }

    func flattenElements() -> [Node] {
        var elements = [Node]()
        deepFirstSearch(flattenElements: &elements, fromNode: self)
        return elements
    }

    func element(byId nodeId: String) -> Node? {
        return element(byId: nodeId, for: Array(children))
    }

    // MARK: - Private
    private func deepFirstSearch(flattenElements: inout [Node], fromNode node: Node) {
        flattenElements.append(node)
        node.children.forEach { deepFirstSearch(flattenElements: &flattenElements, fromNode: $0) }
    }
    
    private func element(byId nodeId: String, for children: [Node]) -> Node? {
        if id == nodeId { return self }
        for i in 0..<children.count {
            if let foundNode = children[i].element(byId: nodeId) {
                return foundNode
            }
        }
        return nil
    }
}
