//
//  NodeTableViewCell.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 25.04.2021.
//

import UIKit

class NodeTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet private weak var nodeNameLabel: UILabel!
    @IBOutlet private weak var depthOffsetConstraint: NSLayoutConstraint!

    // MARK: - Public
    var isDeleted: Bool = false {
        didSet {
            nodeNameLabel.textColor =  isDeleted ? .lightGray : .black
        }
    }

    var nodeName: String? {
        didSet {
            nodeNameLabel.text = nodeName
        }
    }

    var depthOffset: CGFloat = 0 {
        didSet {
            depthOffsetConstraint.constant = depthOffset + 16
        }
    }
}
