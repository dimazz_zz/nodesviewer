//
//  ViewController.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 24.04.2021.
//

import UIKit

class NodesEditViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet private var nodesCashTableView: UITableView!
    @IBOutlet private var nodesDataBaseTableView: UITableView!

    // MARK: - Public
    var viewModel = NodesEditViewModel()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Actions
    @IBAction func insertFromBaseButtonDidPress() {
        if let selectedIndex = nodesDataBaseTableView.indexPathForSelectedRow?.row {
            viewModel.insertNodeFromBase(withSelectedIndex: selectedIndex)
            nodesCashTableView.reloadData()
        }
    }

    @IBAction func addNewNodeButtonDidPress() {
        if let selectedIndex = nodesCashTableView.indexPathForSelectedRow?.row {
            let alert = UIAlertController(title: nil, message: "Введите имя нового элемента", preferredStyle: .alert)
            alert.addTextField()
            alert.addAction(UIAlertAction(title: "Добавить", style: .default, handler: { (_) in
                self.viewModel.addChild(withName: alert.textFields?.first?.text ?? "", forSelectedNodeIndex: selectedIndex)
                self.nodesCashTableView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            present(alert, animated: true)
        }
    }

    @IBAction func removeNodeButtonDidPress() {
        if let selectedIndex = nodesCashTableView.indexPathForSelectedRow?.row {
            viewModel.removeNode(atIndex: selectedIndex)
            nodesCashTableView.reloadData()
        }
    }
    
    @IBAction func renameNodeButtonDidPress() {
        if let selectedIndex = nodesCashTableView.indexPathForSelectedRow?.row {
            let alert = UIAlertController(title: nil, message: "Введите новое имя элемента", preferredStyle: .alert)
            alert.addTextField()
            alert.addAction(UIAlertAction(title: "Переименовать", style: .default, handler: { (_) in
                self.viewModel.renameNode(withName: alert.textFields?.first?.text ?? "", forSelectedNodeIndex: selectedIndex)
                self.nodesCashTableView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            present(alert, animated: true)
        }
    }
    
    @IBAction func applyCacheButtonDidPress() {
        viewModel.applyDatabase()
        nodesDataBaseTableView.reloadData()
        nodesCashTableView.reloadData()
    }
    
    @IBAction func resetAllTreesButtonDidPress() {
        viewModel.resetAllTrees()
        nodesDataBaseTableView.reloadData()
        nodesCashTableView.reloadData()
    }
    
    // MARK: - Private
    private lazy var nodesCacheDataSource = NodesTreeDataSource(dataProvider: viewModel.cacheTreeDataProvider)
    private lazy var nodesDataBaseDataSource = NodesTreeDataSource(dataProvider: viewModel.dataBaseTreeDataProvider)
}

// MARK: - Private methods
private extension NodesEditViewController {
    func setup() {
        nodesDataBaseTableView.dataSource = nodesDataBaseDataSource
        nodesDataBaseTableView.reloadData()
        nodesCashTableView.dataSource = nodesCacheDataSource
    }
}

