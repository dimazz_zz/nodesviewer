//
//  NodesEditViewModel.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 25.04.2021.
//

import Foundation
import RealmSwift

class NodesEditViewModel {
    init() {
        let rootDefaultNode = DatabaseManager.shared.rootNodes
        dataBaseTreeDataProvider = NodesTreeDataProvider(rootNodes: rootDefaultNode.map({ $0.clone()} ))
    }

    var dataBaseTreeDataProvider: NodesTreeDataProvider!
    var cacheTreeDataProvider = NodesTreeDataProvider()
    
    func insertNodeFromBase(withSelectedIndex index: Int) {
        if let selectedNode = dataBaseTreeDataProvider.node(atIndex: index) {
            cacheTreeDataProvider.appendToTree(node: selectedNode)
        }
    }

    func addChild(withName name: String, forSelectedNodeIndex index: Int) {
        cacheTreeDataProvider.addChildNode(withName: name, forSelectedIndex: index)
    }
    
    func renameNode(withName name: String, forSelectedNodeIndex index: Int) {
        cacheTreeDataProvider.renameNode(withName: name, atIndex: index)
    }

    func removeNode(atIndex index: Int) {
        cacheTreeDataProvider.removeNode(atIndex: index)

    }
    
    func resetAllTrees() {
        cacheTreeDataProvider.updateRootNodes(nodes: [])
        DatabaseManager.shared.setupDefaultDatabase()
        dataBaseTreeDataProvider.updateRootNodes(nodes: DatabaseManager.shared.rootNodes.map({ $0.clone()} ))
    }
    
    func applyDatabase() {
        for cacheNode in cacheTreeDataProvider.flattenNodes {
            for databaseNode in dataBaseTreeDataProvider.rootNodes {
                if let foundNode = databaseNode.element(byId: cacheNode.id) {
                    foundNode.update(withNode: cacheNode)
                } else {
                    dataBaseTreeDataProvider.appendToTree(node: cacheNode.clone())
                }
            }
        }
        DatabaseManager.shared.overrideDatabase(withNodes: dataBaseTreeDataProvider.rootNodes)
        let rootDefaultNode = DatabaseManager.shared.rootNodes
        dataBaseTreeDataProvider.updateRootNodes(nodes: rootDefaultNode.map({ $0.clone()} ) )
        cacheTreeDataProvider.flattenNodes.filter( {$0.deleted} ).forEach { node in
            for rootNode in cacheTreeDataProvider.rootNodes {
                if dataBaseTreeDataProvider.isNodeDescendent(rootNode, forForeParent: node) {
                    rootNode.delete()
                }
            }
        }
    }
}
