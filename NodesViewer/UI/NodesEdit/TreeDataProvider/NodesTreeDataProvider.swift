//
//  NodesTreeDataProvider.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 25.04.2021.
//

import Foundation

class NodesTreeDataProvider {
    // MARK: - Public
    convenience init(rootNodes: [Node]) {
        self.init()
        self.rootNodes = rootNodes
        generateFlattenNodes()
    }
    
    private (set) var rootNodes = [Node]()
    private (set) var flattenNodes = [Node]()

    func updateRootNodes(nodes: [Node]) {
        self.rootNodes = nodes
        generateFlattenNodes()
    }

    func addChildNode(withName name: String, forSelectedIndex index: Int) {
        let selectedNode = node(atIndex: index)
        selectedNode?.children.append(Node(id: UUID().uuidString, name: name, parent: selectedNode))
        generateFlattenNodes()
    }
    
    func renameNode(withName name: String, atIndex index: Int) {
        if let selectedNode = node(atIndex: index) {
            selectedNode.name = name
        }
    }

    func appendToTree(node: Node) {
        if !flattenNodes.contains(where: {$0.id == node.id}) {
            let nodeCopy = Node(id: node.id, name: node.name, deleted: node.deleted)
            for flatNode in flattenNodes {
                if node.children.contains(flatNode) {
                    nodeCopy.appendChild(flatNode)
                }
            }
            var nodeAdded = false
            for i in 0..<flattenNodes.count {
                if let parentId = node.parent?.id, let foundNode = flattenNodes[i].element(byId: parentId) {
                    foundNode.appendChild(nodeCopy)
                    if foundNode.deleted {
                        foundNode.delete()
                    }
                    nodeAdded = true
                    break
                }
            }
            if !nodeAdded {
                rootNodes.append(nodeCopy)
            }
            rootNodes.removeAll(where: { nodeCopy.children.contains($0) })
            generateFlattenNodes()
        }
    }
    
    func isNodeDescendent(_ node: Node, forForeParent foreParentNode: Node) -> Bool {
        for rootNode in rootNodes {
            if let foundNode = rootNode.element(byId: node.id) {
                var parentNode = foundNode.parent
                while parentNode != nil {
                    if foreParentNode.id == parentNode?.id {
                        return true
                    }
                    parentNode = parentNode?.parent
                }
                break
            }
        }
        return false
    }

    func removeNode(atIndex index: Int) {
        let nodeToDelete = node(atIndex: index)
        nodeToDelete?.delete()
    }

    var numberOfNodes: Int { flattenNodes.count }

    func node(atIndex index: Int) -> Node? {
        guard index < numberOfNodes else { return nil}
        return flattenNodes[index]
    }
}

// MARK: - Private methods
private extension NodesTreeDataProvider{
    func generateFlattenNodes() {
        flattenNodes.removeAll()
        rootNodes.forEach { flattenNodes.append(contentsOf: $0.flattenElements()) }
    }
}
