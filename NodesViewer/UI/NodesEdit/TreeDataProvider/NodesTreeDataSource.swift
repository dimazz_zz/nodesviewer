//
//  NodesTreeDataSource.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 25.04.2021.
//

import UIKit

class NodesTreeDataSource: NSObject {
    // MARK: - Public
    init(dataProvider: NodesTreeDataProvider) {
        self.dataProvider = dataProvider
    }

    // MARK: - Private
    private let dataProvider: NodesTreeDataProvider
    private let nodeCellIdentifier = "NodeTableViewCell"
}

// MARK: - UITableViewDataSource
extension NodesTreeDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataProvider.numberOfNodes
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nodeCell = tableView.dequeueReusableCell(withIdentifier: nodeCellIdentifier, for: indexPath) as! NodeTableViewCell
        let node = dataProvider.node(atIndex: indexPath.row)
        nodeCell.nodeName = node?.name
        nodeCell.isDeleted = node?.deleted ?? false
        if let depth = node?.depth {
            nodeCell.depthOffset = CGFloat(depth) * 20
        }

        nodeCell.selectionStyle = (node?.deleted ?? false) ? .none : .gray
        nodeCell.isUserInteractionEnabled = !(node?.deleted ?? false)
        return nodeCell
    }
}
