//
//  DatabaseManager.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 12.05.2021.
//

import Foundation
import RealmSwift

class DatabaseManager {
    // MARK: - Public
    static var shared: DatabaseManager = {
        let instance = DatabaseManager()
        return instance
    }()

    var realm = try? Realm()
    
    var rootNodes: [Node] {
        let realm = try! Realm()
        return Array(realm.objects(Node.self).filter("parent == null"))
    }
    
    func overrideDatabase(withNodes nodes: [Node]) {
        try? realm?.write {
            realm?.deleteAll()
            realm?.add(nodes)
        }
    }
    
    func setupDefaultDatabase() {
        let rootDefaultNode = NodeHelper.makeDefaultTree()
        try? realm?.write {
            realm?.deleteAll()
            realm?.add(rootDefaultNode)
        }
    }
    
    static func configureMigration() {
        let config = Realm.Configuration(schemaVersion: 2, migrationBlock: { (migration, oldSchemaVersion) in
            if oldSchemaVersion < 1 {
                self.migrateFrom0To1(with: migration)
            }
            if oldSchemaVersion < 2 {
            }
        })
        Realm.Configuration.defaultConfiguration = config
    }
    
    // MARK: - Migrations
    static func migrateFrom0To1(with migration: Migration) {
        migration.enumerateObjects(ofType: Node.className()) { (_, newNode) in
            newNode?["foreParents"] = []
        }
    }

    // MARK: - Private
    private init() {}
}

// MARK: - NSCopying
extension DatabaseManager: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
}
