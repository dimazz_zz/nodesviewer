//
//  NodeHelper.swift
//  NodesViewer
//
//  Created by Дмитрий Ли on 24.04.2021.
//

import Foundation

class NodeHelper {
    static func makeDefaultTree() -> Node {
        let fifthLevel = [Node(id: UUID().uuidString, name: "Node11")]
        let forthLevel = [Node(id: UUID().uuidString, name: "Node10", children: fifthLevel)]
        let thirdLevel3 = [Node(id: UUID().uuidString, name: "Node8", children: forthLevel), Node(id: UUID().uuidString, name: "Node9")]
        let thirdLevel2 = [Node(id: UUID().uuidString, name: "Node6")]
        let thirdLevel = [Node(id: UUID().uuidString, name: "Node4")]
        let secondLevel = [Node(id: UUID().uuidString, name: "Node2"), Node(id: UUID().uuidString, name: "Node3", children: thirdLevel3), Node(id: UUID().uuidString, name: "Node5", children: thirdLevel2), Node(id: UUID().uuidString, name: "Node7", children: thirdLevel)]
        return Node(id: UUID().uuidString, name: "Node1", children: secondLevel)
    }
}
