//
//  NodeTests.swift
//  NodesViewerTests
//
//  Created by Дмитрий Ли on 12.05.2021.
//

import XCTest

@testable import NodesViewer
class NodeTests: XCTestCase {
    func testFirstDepthLevel() {
        let rootNode = makeDefaultTree()
        XCTAssert(rootNode.depth == 0)
    }
    
    func testSecondDepthLevel() {
        let rootNode = makeDefaultTree()
        XCTAssert(rootNode.element(byId: "Node2id")?.depth == 1)
    }
    
    func testThirdDepthLevel() {
        let rootNode = makeDefaultTree()
        XCTAssert(rootNode.element(byId: "Node3id")?.depth == 2)
    }
    
    func testForthDepthLevel() {
        let rootNode = makeDefaultTree()
        XCTAssert(rootNode.element(byId: "Node4id")?.depth == 3)
    }
    
    func testFifthDepthLevel() {
        let rootNode = makeDefaultTree()
        XCTAssert(rootNode.element(byId: "Node5id")?.depth == 4)
    }
    
    func testDeleteRoot() {
        let rootNode = makeDefaultTree()
        rootNode.delete()
        XCTAssert(rootNode.deleted)
    }
    
    func testDeleteChildren() {
        let rootNode = makeDefaultTree()
        rootNode.delete()
        var isAllDeleted = true
        rootNode.children.forEach { isAllDeleted = isAllDeleted && $0.deleted }
        XCTAssert(isAllDeleted)
    }
    
    func testFindElement() {
        let rootNode = makeDefaultTree()
        XCTAssert(rootNode.element(byId: "Node3id")?.name == "Node3")
    }
    
    func testFlattenElement() {
        let rootNode = makeTreeForFlatten()
        XCTAssert(rootNode.flattenElements().map { $0.name } == ["Node1", "Node2", "Node5", "Node3", "Node4"])
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    private  func makeDefaultTree() -> Node {
        let fifthLevel = [Node(id: "Node5id", name: "Node5")]
        let forthLevel = [Node(id: "Node4id", name: "Node4", children: fifthLevel)]
        let thirdLevel = [Node(id: "Node3id", name: "Node3", children: forthLevel)]
        let secondLevel = [Node(id: "Node2id", name: "Node2", children: thirdLevel)]
        return Node(id: "Node1id", name: "Node1", children: secondLevel)
    }
    
    private func makeTreeForFlatten() -> Node {
        let thirdLevel2 = [Node(id: UUID().uuidString, name: "Node5")]
        let thirdLevel = [Node(id: UUID().uuidString, name: "Node4")]
        let secondLevel = [Node(id: UUID().uuidString, name: "Node2", children: thirdLevel2), Node(id: UUID().uuidString, name: "Node3", children: thirdLevel)]
        return Node(id: UUID().uuidString, name: "Node1", children: secondLevel)
    }
}
