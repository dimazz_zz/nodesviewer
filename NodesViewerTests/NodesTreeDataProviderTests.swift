//
//  NodesTreeDataProviderTests.swift
//  NodesViewerTests
//
//  Created by Дмитрий Ли on 13.05.2021.
//

import XCTest

@testable import NodesViewer
class NodesTreeDataProviderTests: XCTestCase {
    var dataProvider: NodesTreeDataProvider!

    func testAddNodeWithoutRelations() {
        dataProvider = NodesTreeDataProvider(rootNodes: [makeDefaultTree()])
        let newNode = Node(id: UUID().uuidString, name: "NewNode")
        dataProvider.appendToTree(node: newNode)
        XCTAssert(dataProvider.rootNodes.contains(where: { $0.name == "NewNode" }))
    }
    
    func testAddNodeWithParent() {
        let rootNode = makeDefaultTree()
        dataProvider = NodesTreeDataProvider(rootNodes: [rootNode])
        let newNode = Node(id: UUID().uuidString, name: "NewNode", parent: rootNode.element(byId: "Node3ID"))
        dataProvider.appendToTree(node: newNode)
        let parentNode = dataProvider.rootNodes.first?.element(byId: "Node3ID")
        XCTAssert(parentNode?.children.contains(where: { $0.name == "NewNode" }) ?? false)
    }
    
    func testAddNodeWithParentAndChild() {
        let rootNodes = [Node(id: "Node1ID", name: "Node1"),
                         Node(id: "Node2ID", name: "Node2")]
        dataProvider = NodesTreeDataProvider(rootNodes: rootNodes)
        let parentNode = rootNodes.first
        let newNode = Node(id: UUID().uuidString, name: "NewNode",
                           children: [rootNodes.last].compactMap{$0},
                           parent: parentNode)
        dataProvider.appendToTree(node: newNode)
        XCTAssert(parentNode?.flattenElements().map { $0.name } == ["Node1", "NewNode", "Node2"])
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    private func makeDefaultTree() -> Node {
        let fifthLevel = [Node(id: "Node11ID", name: "Node11")]
        let forthLevel = [Node(id: "Node10ID", name: "Node10", children: fifthLevel)]
        let thirdLevel3 = [Node(id: "Node8ID", name: "Node8", children: forthLevel), Node(id: "Node9ID", name: "Node9")]
        let secondLevel = [Node(id: "Node2ID", name: "Node2"), Node(id: "Node3ID", name: "Node3", children: thirdLevel3)]
        return Node(id: "Node1ID", name: "Node1", children: secondLevel)
    }
}
